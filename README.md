# Installation
You need [BepInEx](https://youtu.be/meNiXcbPh_s) to use this mod.
Put the UKScreenshot.dll in BepInEx/plugins folder.

# Usage
Use F2 to take a screenshot with your weapons, or press F3 to take a screenshot with no weapons.
The mod automatically hides the necessary UI elements, with the exception of Style Meter.

# Screenshots
![screenshot](https://i.imgur.com/vvsnaZ6.png)
![screenshot](https://i.imgur.com/Oq2RIpA.png)
![screenshot](https://i.imgur.com/SxVEtr9.png)
![screenshot](https://i.imgur.com/caW9JyN.png)
![screenshot](https://i.imgur.com/kbqg9yP.png)
![screenshot](https://i.imgur.com/q6WPdiA.png)
![screenshot](https://i.imgur.com/58qxdwh.png)
