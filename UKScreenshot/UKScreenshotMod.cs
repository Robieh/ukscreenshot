﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using BepInEx;
using UnityEngine;

namespace UKScreenshot {
    [BepInPlugin(pluginGuid, pluginName, pluginVersion)]
    public class UKScreenshotMod : BaseUnityPlugin {
        public const string pluginGuid = "robiehh.uk.screenshot";
        public const string pluginName = "UKScreenshot";
        public const string pluginVersion = "1.0";

        private bool _reenableStuff;
        private int _crosshairType;
        private int _hudType;
        private bool _styleMeter;
        private GameObject _gunControl;
        private List<BossHealthBar> _bossList;

        void Awake() {
            _reenableStuff = false;
            _bossList = new List<BossHealthBar>();
        }

        public void Update() {                
            var hud = MonoSingleton<HUDOptions>.Instance;
            if (_reenableStuff) {
                _reenableStuff = false;
                hud.CrossHairType(_crosshairType);
                hud.HudType(_hudType);
                hud.StyleMeter(_styleMeter);
                hud.StyleInfo(_styleMeter);
                hud.gameObject.SetActive(true);
                if(_gunControl != null) _gunControl.SetActive(true);
                foreach (BossHealthBar bhb in _bossList) {
                    bhb.enabled = true;
                    bhb.SendMessage("Awake");
                }
            }
            
            if (Input.GetKeyDown(KeyCode.F2) || Input.GetKeyDown(KeyCode.F3)) {
                _crosshairType = PlayerPrefs.GetInt("CroHai");
                _hudType = PlayerPrefs.GetInt("AltHud");
                _styleMeter = PlayerPrefs.GetInt("StyMet") == 1;
                _bossList.Clear();
                foreach (BossHealthBar bhb in FindObjectsOfType<BossHealthBar>()) {
                    bhb.enabled = false;
                }
                hud.CrossHairType(0);
                hud.HudType(0);
                if (Input.GetKeyDown(KeyCode.F3)) {
                    hud.StyleMeter(false);
                    hud.StyleInfo(false);
                    hud.gameObject.SetActive(false);
                    _gunControl = FindObjectOfType<GunControl>().gameObject;
                    _gunControl.SetActive(false);
                }
                var folder = Path.GetFullPath(Path.Combine(Path.Combine(Application.dataPath, @"../"), @"Screenshots"));
                var path = Path.Combine(folder, @"Screenshot_" + System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".png");
                Directory.CreateDirectory(folder);
                ScreenCapture.CaptureScreenshot(path, 2);
                _reenableStuff = true;
                Logger.LogInfo($"Screenshot saved to {path}");
            }
        }
    }
}